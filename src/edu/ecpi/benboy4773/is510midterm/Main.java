package edu.ecpi.benboy4773.is510midterm;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		if (args.length == 0){
			System.out.println("Please supply a number of anthills to create.");
			return;
		}
		try {
			
			int hills = Integer.parseInt(args[0]);
			Anthill[] anthills = new Anthill[hills];
			
			for (int i = 0; i < hills; i++){
				//Constructor randomizes type, adult count and larvae count 
				anthills[i] = new Anthill();
			}
			
			for (int i = 0; i < hills; i++){
				System.out.println(anthills[i].toString());
			}

		} catch (NumberFormatException e) {
			System.out.println("Parameter must be an integer.");
		}
	}

}
