package edu.ecpi.benboy4773.is510midterm;

import java.util.Random;

public class Anthill {

	public enum AntType {Acrobat, Carpenter, Field, Fire, Harvester, Moisture, Odorous};

	protected AntType antType;
	protected int numberOfLarvae;
	protected int numberOfAdults;

	public AntType getAntType() {
		return antType;
	}

	public void setAntType(AntType antType) {
		this.antType = antType;
	}

	public int getNumberOfLarvae() {
		return numberOfLarvae;
	}

	public void setNumberOfLarvae(int numberOfLarvae) {
		this.numberOfLarvae = numberOfLarvae;
	}

	public int getNumberOfAdults() {
		return numberOfAdults;
	}

	public void setNumberOfAdults(int numberOfAdults) {
		this.numberOfAdults = numberOfAdults;
	}

	public Anthill() {
		Random r = new Random();
		
		//random type
		int max = AntType.values().length;// max based on ant types
		this.setAntType(AntType.values()[r.nextInt(max)]);

		//random adults
		this.setNumberOfAdults(r.nextInt(1000));

		//random larvae
		this.setNumberOfLarvae(r.nextInt(500));//population control ;)
	}
	
	public String toString(){
		StringBuffer result = new StringBuffer("");
		result.append("{\n");
		result.append("	\"type\": \"" + this.getAntType().toString() + "\",\n");
		result.append("	\"adults\": " + this.getNumberOfAdults() + ",\n");
		result.append("	\"larvae\": " + this.getNumberOfLarvae() + ",\n");
		result.append("}");

		return result.toString();
	}

}